# phonegap-assets

This tool will take an image (1024*1024[min]) then generate a set of appropriate sizes icons for your Phonegap project.

There are many of those out there, but more often then not, they are useless. And Phonegap/Cordova keep changing their names and cause
hell for developer. So why not use the config.xml as the starting point and using what is there to generate the assets you need?

Hence this little tool

## Installation

It's recommended to install this globally

```sh
$ npm install phonegap-assets --global
```

Then you can use it like this

```sh
$ phonegap-assets --config path/to/config.xml
```

It will read your Phonegap `config.xml` file, and start generate icons/splash base of the info.

## Options

* source (-s) the path to the asset
* dest (-d) where the output will be
* icon (-i) the name of the icon file (default: `icon.png`)
* splash (-sp) the prefix of the splash file (default: `splash`)
* config (-c) the full path with name to the `config.xml`
* no-round (-nr) for Android
* round (-r) for iOS 

---

MIT (c) 2020 Joel Chu
