#!/usr/bin/env node

const meow = require('meow')
const { join } = require('path')
const development = process.env.NODE_ENV === 'development'
const fixtures = join('tests', 'fixtures')
const phonegapAssetsGenerator = require('./index')
const cli = meow(`
	Usage
	  $ phonegap-assets

	Options
	  --config, -c relative path to config.xml

	Examples
	  $ phonegap-assets --config path/to/config.xml

`, {
	flags: {
    source: {
      type: 'string',
      alias: 's',
      default: development ? join(fixtures, 'src', 'assets') : 'src/assets'
    },
		dest: {
			type: 'string',
			alias: 'd',
			default: development ? fixtures : ''
		},
    icon: {
      type: 'string',
      alias: 'i',
      default: 'icon.png'
    },
    splash: {
      type: 'string',
      alias: 'sp',
      default: 'splash' // this one is prefix
    },
		config: {
			type: 'string',
			alias: 'c',
      default: development ? join(fixtures, 'config.xml') : 'config.xml'
		},
    'no-round': {
      type: 'boolean',
      alias: 'nr',
      default: false
    },
    'round': {
      type: 'array',
      alias: 'r',
      default: ['ios']
    }
  }
})
// run it 
phonegapAssetsGenerator(cli.flags)
