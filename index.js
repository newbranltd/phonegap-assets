/**
 * phonegap-assets
 * main interface for you to include it to use it programmatically
 */
const _ = require('lodash')
const fsx = require('fs-extra')
const { inspect } = require('util')
const { join } = require('path')
const {
  ext,
  validate,
  xmlToJson,
  iconGenerator,
  splashGenerator
} = require('./src')

const debug = (title, data) => {
  console.log(title);
  console.log(inspect(data, false, null))
}

/**
 * @param {int} width width of the output
 * @param {int} height height of the output
 * @return {string} portrait or landscape
 */
const getStyle = splash => {
  const { width, height } = splash
  splash.style = (parseInt(width, 10) > parseInt(height,10)) ? 'port' : 'land'
  return splash
}

/**
 * main export
 * @param {object} params required parameters
 * @return {void} nothing in return
 * @public
 */
module.exports = async function generator(params) {
  const { file, source, dest, round, icon, splash } = validate(params)
  // grabbing the data
  const files = await xmlToJson(file)
  // debug('FILES', files);
  _.forEach(files, (f, p) => {
    if (f.icon) {
      const toRound = round.indexOf(p) > -1;
      iconGenerator(join(source, icon), f.icon, dest, p, toRound)
    }
    if (f.splash) {
      splashGenerator(splash, p, dest, f.splash.map(getStyle))
    }
  })
}
