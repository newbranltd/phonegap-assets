/**
 * Android don't provide their width and height so we need to translate them
 */

module.exports = {
  ldpi: {width: 36, height: 36},
  mdpi: {width: 48, height: 48},
  hdpi: {width: 72, height: 72},
  xhdpi: {width: 96, height: 96},
  xxhdpi: {width: 144, height: 144},
  xxxhdpi: {width: 192, height: 192},
  'land-ldpi': {width: 426, height: 320},
  'land-mdpi': {width: 470, height: 320},
  'land-hdpi': {width: 640, height: 480},
  'land-xhdpi': {width: 960, height: 720},
  'land-xxhdpi': {width: 1600, height: 960},
  'land-xxxhdpi': {width: 1920, height: 1280},
  'port-ldpi': {width: 320, height: 426},
  'port-mdpi': {width: 320, height: 470},
  'port-hdpi': {width: 480, height: 640},
  'port-xhdpi': {width: 720, height: 960},
  'port-xxhdpi': {width: 960, height: 1600},
  'port-xxxhdpi': {width: 1280, height: 1920}
}
