// util method to feed back
const colors = require('colors')
const { inspect } = require('util')
/**
 * @var {Object} console utils
 */
var display = {}

display.success = function (str) {
    str = '✓  '.green + str;
    console.log('  ' + str);
}

display.error = function (str) {
    str = '✗  '.red + str
    console.log('  ' + str)
}

display.header = function (str) {
    console.log('')
    console.log(' ' + str.cyan.underline)
    console.log('')
}

display.inspect = function(obj) {
  console.log(inspect(obj, false, null))
}

module.exports = display
