// #!/usr/bin/env node
// require('events').EventEmitter.defaultMaxListeners = 0;

///////////////////////////////////////////////////////////////////////
const fs = require("fs")
const { dirname, join } = require("path")
const sharp = require("sharp")
const rimraf = require("rimraf")
const fsx = require('fs-extra')
const { success, error, inspect, header } = require('./display')

// export
module.exports = function iconGenerator(sourceFile, images, destDir, platform, roundCorners = false) {

  let pipeline = sharp(sourceFile).metadata((err, meta) => {
    if (err) {
      error(`Error while reading \"${sourceFile}\"`, err)
      return 1
    }

    if (meta.width !== meta.height) {
      error('Source file is not square.')
      return 1
    }
    let iconType = 'Square'
    if (roundCorners) {
      iconType = 'Round'
      // http://stackoverflow.com/questions/31255291/android-launcher-icon-rounded-corner-edge-radii#
      // https://material.io/guidelines/style/icons.html#icons-system-icons
      // 8.33%
      let radius = meta.width * 0.0833
      let svgBuffer = new Buffer(
        `<svg><rect x="0" y="0" width="${meta.width}" height="${meta.height}" rx="${radius}" ry="${radius}"/></svg>`
      )
      pipeline = pipeline.overlayWith(svgBuffer, { cutout: true })
    }

    pipeline.toBuffer((err, sourceBuffer) => {
      if (err) {
        error(`Error constructing in-memory buffer of \"${sourceFile}\"`, err)
        process.exit()
      }
      header(`Generate icon for ${platform}`)
      images.forEach(function (image) {
        const { src, width, height } = image
        const dest = join(destDir, src)
        const dir = dirname(dest)
        // check if it exits then remove it
        if (fsx.pathExistsSync(dest)) {
          rimraf.sync(dest)
        } else if (!fsx.pathExistsSync(dir)) {
          fsx.ensureDirSync(dir)
        }
        // do it
        sharp(sourceBuffer)
          .resize({ width, height })
          .toFile(dest, function (err, info) {
            if (err) {
              header(`Error while processing image`)
              inspect(err)
              return 1
            }
            success(`${iconType} ${platform} icon created in ${dest}`)
          })
      })
    })
  })
}
