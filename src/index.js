// the main interface to export functions
const xmlToJson = require('./xml-to-json')
const iconGenerator = require('./icon-generator')
const { validate, ext } = require('./validates')
const splashGenerator = require('./splash-generator')
// export
module.exports = {
  xmlToJson,
  iconGenerator,
  splashGenerator,
  validate,
  ext
}
