// Splash generator
const _ = require('lodash')
const fs = require('fs')
const sharp = require("sharp")
const { success, error, header, inspect } = require('./display')
const { dirname, join } = require("path")
const rimraf = require("rimraf")
const fsx = require('fs-extra')
const styles = {port: 'portrait', land: 'landscape'}
/**
 * @param {array} sourceFile the portrait and landscape splashes
 * @param {array} splashes src, dimensions, port or land
 * @param {string} dest prefix the dest directory
 * @param {string} splashType portrait or landscape
 * @param {string} platform names
 * @return {undefined}
 */
const splashGeneratorForPlatform = function(sourceFile, splashes, destDir, splashType, platform) {
  let pipeline = sharp(sourceFile).metadata((err, meta) => {
    if (err) {
      error(`Error while reading \"${sourceFile}\"`, err)
      return 1
    }
    pipeline.toBuffer((err, sourceBuffer) => {
      if (err) {
        error(`Error constructing in-memory buffer of \"${sourceFile}\"`, err)
        process.exit()
      }

      header(`Generate ${splashType} splash for ${platform}`)

      splashes.forEach(function (image) {
        const { src, width, height } = image
        const dest = join(destDir, src)
        const dir = dirname(dest)
        // check if it exits then remove it
        if (fsx.pathExistsSync(dest)) {
          rimraf.sync(dest)
        } else if (!fsx.pathExistsSync(dir)) {
          fsx.ensureDirSync(dir)
        }
        // do it
        sharp(sourceBuffer)
          .resize({ width, height })
          .toFile(dest, function (err, info) {
            if (err) {
              header(`Error while processing image`)
              inspect(err)
              return 1
            }
            success(`${platform} splash screen created in ${dest}`)
          })
      })
    })
  })
}

/**
 * @param {string} source directory
 * @param {object} splash source files
 * @param {string} platform name
 * @param {string} dest extra path to dest directory
 * @param {array} splashes array of objects
 * @return {undefined} nothing
 * @public
 */
module.exports = function splashGenerator(splash, platform, dest, splashes) {
  _.forEach(splash, (src, style) => {
    const _style = styles[style]
    splashGeneratorForPlatform(
      src,
      splashes.filter(s => s.style === style),
      dest,
      _style,
      platform
    )
  })
}
