// validation and get paths functions
const _ = require('lodash')
const fsx = require('fs-extra')
const { join } = require('path')
const ext = 'png' // MUST BE A PNG FILE
const cwd = process.cwd()



/**
 * create a name
 * @param {string} prefix to prefix
 * @param {string} suffix to suffix
 * @return {string} filename
 */
const getFilename = function(prefix, suffix) {
  return [
    [
      prefix,
      suffix
    ].join('-'),
    ext
  ].join('.')
}

/**
 * find the splash files
 * @param {string} prefix the splash file prefix
 * @param {string} source where it is
 * @return {object} portrait / landscape paths
 */
const getSplashFiles = function(prefix, source) {
  const splash = join(source, [prefix, ext].join('.'))
  const target = {
    port: join(source, getFilename(prefix, 'port')),
    land: join(source, getFilename(prefix, 'land'))
  }
  if (fsx.pathExistsSync(target.port) && fsx.pathExistsSync(land)) {
    return target
  }
  if (fsx.pathExistsSync(splash)) {
    return {port: splash, land: splash}
  }
  throw new Error('Splash file(s) not found!!!')
}

/**
 * validate the input before we start processing the files
 * @param {object} params flags via meow
 * @return {object} options to work with
 */
const validate = params => {
  if (_.isEmpty(params.config)) {
    throw new Error('config.xml is undefined!!!')
  }
  // this way we allow the user to pass an absolute path
  let file = params.config;
  if (!fsx.pathExistsSync(file)) {
    file = join(cwd, params.config)
    if (!fsx.pathExistsSync(file)) {
      throw new Error('config.xml file not found at ' + file)
    }
  }
  if (_.isEmpty(params.source)) {
    throw new Error('Source directory is undefined!!!')
  }
  let source = params.source;
  if (!fsx.pathExistsSync(source)) {
    source = join(cwd, source)
    if (!fsx.pathExistsSync(source)) {
      throw new Error('Source directory not found at ' + source)
    }
  }
  if (_.isEmpty(params.icon)) {
    throw new Error('icon is undefined!!!')
  }
  if (_.isEmpty(params.splash)) {
    throw new Error('splash prefix is undefined!!!')
  }
  return {
    file,
    source,
    round: params.round && _.isArray(params.round) ? params.round : [],
    dest: params.dest,
    icon: params.icon,
    splash: getSplashFiles(params.splash, source)
  }
}

module.exports = {
  ext,
  validate
}
