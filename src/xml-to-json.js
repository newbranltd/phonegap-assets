// read the xml files
const _ = require('lodash')
const fsx = require('fs-extra')
const fs = require('fs')
const xml2js = require('xml2js')
const androidSizeTable = require('./android-sizes')
// should be in it's own file
const PLATFORM_ANDROID = 'android'

/**
 * Xml parse to json
 * @param {string} data source
 * @return {object} json
 */
const xml2json = function(data) {
  return new Promise((resolver, rejecter) => {
    const parser = new xml2js.Parser()
    parser.parseString(data, function(err, result) {
      if (err) {
        return rejecter(err)
      }
      resolver(result)
    })
  })
}

const processAndroidSize = function(result) {
  let base = {}
  if (result.icon) {
    base.icon = result.icon.map(icon => (
      _.extend({
        src: icon.$.src
      }, androidSizeTable[icon.$.density])
    ))
  }
  if (result.splash) {
    base.splash = result.splash.map(splash => (
      _.extend({
        src: splash.$.src,
      }, androidSizeTable[splash.$.density])
    ))
  }
  return {android: base}
}

/**
 * check what platforms and what need to generate
 * @param {array} result the xml 2 json result
 * @return {object} processed result
 */
const processPlatform = function(result) {
  result = _.isArray(result) ? result : []
  return result.map(r => {
    const os = r.$.name;
    if ((r.icon && _.isArray(r.icon)) || (r.splash && _.isArray(r.splash))) {
      if (os === PLATFORM_ANDROID) {
        return processAndroidSize(r)
      }
      let base = {};
      if (r.icon) {
        base.icon = r.icon.map(icon => icon.$)
      }
      if (r.splash) {
        base.splash = r.splash.map(splash => splash.$)
      }
      return {[os]: base}
    }
    return {}; // just return empty object if there is none
  }).reduce((last, next) => {
    return _.merge(last, next)
  }, {})
}

// export
module.exports = function xmlToJsonFn(source) {
  return new Promise((resolver, rejecter) => {
    if (!fsx.pathExistsSync(source)) {
      return rejecter('file ' + source + ' not found!')
    }
    const data = fs.readFileSync(source, {encoding: 'utf-8'})
    xml2json(data)
      .then(result => processPlatform(result.widget.platform))
      .then(resolver)
      .catch(rejecter)
  })
}
